provider "aws" {
  assume_role {
    role_arn = var.log_account_role_arn
  }
  alias  = "log_account"
  region = var.log_account_region
}

provider "aws" {
  assume_role {
    role_arn = var.prod_account_role_arn
  }
  alias  = "prod_account"
  region = var.prod_account_region
}

module "cloudtrail_s3" {
  source = "../../"

  providers = {
    aws.prod_account = aws.prod_account
    aws.log_account  = aws.log_account
  }

  namespace                                = "keltio"
  stage                                    = "dev"
  s3_bucket_name                           = "cloudtrail_log"
  s3_bucket_force_destroy                  = true
  cloudtrail_name                          = "CloudTrail"
  cloudtrail_enable_log_file_validation    = true
  cloudtrail_include_global_service_events = true
  cloudtrail_is_multi_region_trail         = true
  cloudtrail_enable_logging                = true

  cloudtrail_bucket_tags = {
    "Name" = "Test_Cloudtrail"
  }

  s3_bucket_tags = {
    "Name" = "Test_S3_bucket_for_cloudtrail"
  }

}
