variable "log_account_role_arn" {
  type        = string
  description = "ARN of IAM role to be assumed to access to the Log Account"
}

variable "prod_account_role_arn" {
  type        = string
  description = "ARN of IAM role to be assumed to access to the Prod Account"
}

variable "log_account_region" {
  type        = string
  description = "Region of your AWS Log account e.g. us-east-1"
}

variable "prod_account_region" {
  type        = string
  description = "Region of your AWS Production account e.g. us-east-1"
}