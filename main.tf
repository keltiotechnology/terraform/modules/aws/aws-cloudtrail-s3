module "s3_bucket" {
  source = "cloudposse/cloudtrail-s3-bucket/aws"

  providers = {
    aws = aws.log_account
  }

  version                                = "0.20.0"
  namespace                              = var.namespace
  stage                                  = var.stage
  name                                   = var.s3_bucket_name
  force_destroy                          = var.s3_bucket_force_destroy
  abort_incomplete_multipart_upload_days = var.s3_bucket_abort_incomplete_multipart_upload_days
  acl                                    = var.s3_bucket_acl
  allow_ssl_requests_only                = var.s3_bucket_allow_ssl_requests_only
  block_public_acls                      = var.s3_bucket_block_public_acls
  block_public_policy                    = var.s3_bucket_block_public_policy
  bucket_notifications_enabled           = var.s3_bucket_notifications_enabled
  create_access_log_bucket               = var.s3_bucket_create_access_log_bucket
  enable_glacier_transition              = var.s3_bucket_enable_glacier_transition
  expiration_days                        = var.s3_bucket_expiration_days
  glacier_transition_days                = var.s3_bucket_glacier_transition_days
  ignore_public_acls                     = var.s3_bucket_ignore_public_acls
  kms_master_key_arn                     = var.s3_bucket_kms_master_key_arn
  lifecycle_prefix                       = var.s3_bucket_lifecycle_prefix
  lifecycle_rule_enabled                 = var.s3_bucket_lifecycle_rule_enabled
  lifecycle_tags                         = var.s3_bucket_lifecycle_tags
  restrict_public_buckets                = var.s3_bucket_restrict_public_buckets
  tags                                   = var.s3_bucket_tags
}

module "cloudtrail" {
  source = "cloudposse/cloudtrail/aws"

  providers = {
    aws = aws.prod_account
  }

  version                       = "0.20.1"
  namespace                     = var.namespace
  stage                         = var.stage
  name                          = var.cloudtrail_name
  enable_log_file_validation    = var.cloudtrail_enable_log_file_validation
  include_global_service_events = var.cloudtrail_include_global_service_events
  is_multi_region_trail         = var.cloudtrail_is_multi_region_trail
  enable_logging                = var.cloudtrail_enable_logging
  cloud_watch_logs_group_arn    = var.cloudtrail_cloud_watch_logs_group_arn
  cloud_watch_logs_role_arn     = var.cloudtrail_cloud_watch_logs_role_arn
  event_selector                = var.cloudtrail_event_selector
  is_organization_trail         = var.cloudtrail_is_organization_trail
  sns_topic_name                = var.cloudtrail_sns_topic_name
  kms_key_arn                   = var.cloudtrail_kms_key_arn
  tags                          = var.cloudtrail_bucket_tags
  s3_bucket_name                = module.s3_bucket.bucket_id
}
