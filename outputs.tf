output "cloudtrail_arn" {
  description = "ARN of the created Cloudtrail resource"
  value       = module.cloudtrail.cloudtrail_arn
}

output "cloudtrail_home_region" {
  description = "The region in which the Cloudtrail resource was created"
  value       = module.cloudtrail.cloudtrail_home_region
}

output "cloudtrail_id" {
  description = "Name of the created Cloudtrail resource"
  value       = module.cloudtrail.cloudtrail_id
}

output "s3_bucket_arn" {
  description = "ARN of the S3 bucket storing Cloudtrail log"
  value       = module.s3_bucket.bucket_arn
}

output "s3_bucket_domain_name" {
  description = "FQDN of the S3 bucket storing Cloudtrail log"
  value       = module.s3_bucket.bucket_arn
}

output "s3_bucket_id" {
  description = "ID of the S3 bucket storing Cloudtrail log"
  value       = module.s3_bucket.bucket_arn
}

output "s3_bucket_notifications_sqs_queue_arn" {
  description = "Notifications SQS queue ARN of the S3 bucket storing Cloudtrail log"
  value       = module.s3_bucket.bucket_notifications_sqs_queue_arn
}

output "s3_bucket_prefix" {
  description = "Prefix configured for lifecycle rules of the S3 bucket storing Cloudtrail log"
  value       = module.s3_bucket.prefix
}
