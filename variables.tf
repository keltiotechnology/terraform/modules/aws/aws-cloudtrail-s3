/* ------------------------------------------------------------------------------------------------------------ */
/* Context Variables                                                                                            */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type        = string
  description = "Namespace - usually abbreviation of your organization name. It's included in the ID element to ensure resource name globally unique."
}

variable "stage" {
  type        = string
  description = "Stage - usually indicate role e.g. 'prod', 'staging' . It's included in the ID element to ensure resource name globally unique."
}

/* ------------------------------------------------------------------------------------------------------------ */
/* S3 Bucket Variables                                                                                          */
/* ------------------------------------------------------------------------------------------------------------ */
variable "s3_bucket_force_destroy" {
  type        = bool
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable"
  default     = false
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of S3 bucket to store Cloudtrail logs"
}

variable "s3_bucket_tags" {
  type        = map(string)
  description = "Tags attached to S3 bucket"
  default     = {}
}

variable "cloudtrail_name" {
  type        = string
  description = "Name of the trail"
}

# Below are some S3 optional variables
variable "s3_bucket_abort_incomplete_multipart_upload_days" {
  type        = number
  default     = 5
  description = "Maximum time (in days) that you want to allow multipart uploads to remain in progress"
}


variable "s3_bucket_acl" {
  type        = string
  description = "The canned ACL to apply. We recommend log-delivery-write for compatibility with AWS services"
  default     = "log-delivery-write"
}

variable "s3_bucket_allow_ssl_requests_only" {
  type        = bool
  default     = false
  description = "Set to `true` to require requests to use Secure Socket Layer (HTTPS/SSL). This will explicitly deny access to HTTP requests"
}

variable "s3_bucket_block_public_acls" {
  type        = bool
  default     = true
  description = "Set to `false` to disable the blocking of new public access lists on the bucket"
}

variable "s3_bucket_block_public_policy" {
  type        = bool
  default     = true
  description = "Set to `false` to disable the blocking of new public policies on the bucket"
}

variable "s3_bucket_notifications_enabled" {
  type        = bool
  description = "Send notifications for the object created events. Used for 3rd-party log collection from a bucket. This does not affect access log bucket created by this module. To enable bucket notifications on the access log bucket, create it separately using the cloudposse/s3-log-storage/aws"
  default     = false
}

variable "s3_bucket_create_access_log_bucket" {
  type        = bool
  default     = false
  description = "A flag to indicate if a bucket for s3 access logs should be created"
}

variable "s3_bucket_enable_glacier_transition" {
  type        = bool
  default     = false
  description = "Glacier transition might just increase your bill. Set to false to disable lifecycle transitions to AWS Glacier."
}

variable "s3_bucket_expiration_days" {
  description = "Number of days after which to expunge the objects"
  default     = 90
}

variable "s3_bucket_glacier_transition_days" {
  description = "Number of days after which to move the data to the glacier storage tier"
  default     = 60
}

variable "s3_bucket_ignore_public_acls" {
  type        = bool
  default     = true
  description = "Set to `false` to disable the ignoring of public access lists on the bucket"
}

variable "s3_bucket_kms_master_key_arn" {
  type        = string
  description = "The AWS KMS master key ARN used for the SSE-KMS encryption. This can only be used when you set the value of sse_algorithm as aws:kms. The default aws/s3 AWS KMS master key is used if this element is absent while the sse_algorithm is aws:kms"
  default     = ""
}

variable "s3_bucket_lifecycle_prefix" {
  type        = string
  description = "Prefix filter. Used to manage object lifecycle events"
  default     = ""
}

variable "s3_bucket_lifecycle_rule_enabled" {
  type        = bool
  description = "Enable lifecycle events on this bucket"
  default     = true
}

variable "s3_bucket_lifecycle_tags" {
  type        = map(string)
  description = "Tags filter. Used to manage object lifecycle events"
  default     = {}
}

variable "s3_bucket_restrict_public_buckets" {
  type        = bool
  default     = true
  description = "Set to `false` to disable the restricting of making the bucket public"
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Cloudtrail variables                                                                                         */
/* ------------------------------------------------------------------------------------------------------------ */
variable "cloudtrail_enable_log_file_validation" {
  type        = bool
  description = "Enable Cloudtrail log file integrity validation"
  default     = false
}

variable "cloudtrail_include_global_service_events" {
  type        = bool
  description = "Whether the trail is publishing events from global services such as IAM to the log files"
  default     = true
}

variable "cloudtrail_is_multi_region_trail" {
  type        = bool
  description = "Whether the trail is created in the current region or in all regions"
  default     = false
}

variable "cloudtrail_enable_logging" {
  type        = bool
  description = "Enables logging for the trail. Setting this to false will pause logging."
  default     = true
}

variable "cloudtrail_bucket_tags" {
  type        = map(string)
  description = "Tags attached to Cloudtrail"
  default     = {}
}

# Below are some cloudtrail optional variables
variable "cloudtrail_cloud_watch_logs_group_arn" {
  type        = string
  description = "Specifies a log group name using an Amazon Resource Name (ARN), that represents the log group to which CloudTrail logs will be delivered"
  default     = ""
}

variable "cloudtrail_cloud_watch_logs_role_arn" {
  type        = string
  description = "Specifies the role for the CloudWatch Logs endpoint to assume to write to a user’s log group"
  default     = ""
}

variable "cloudtrail_event_selector" {
  type = list(object({
    include_management_events = bool
    read_write_type           = string

    data_resource = list(object({
      type   = string
      values = list(string)
    }))
  }))

  description = "Specifies an event selector for enabling data event logging. See: https://www.terraform.io/docs/providers/aws/r/cloudtrail.html for details on this variable"
  default     = []
}

variable "cloudtrail_is_organization_trail" {
  type        = bool
  default     = false
  description = "The trail is an AWS Organizations trail"
}

variable "cloudtrail_sns_topic_name" {
  type        = string
  description = "Specifies the name of the Amazon SNS topic defined for notification of log file delivery"
  default     = null
}

variable "cloudtrail_kms_key_arn" {
  type        = string
  description = "Specifies the KMS key ARN to use to encrypt the logs delivered by CloudTrail"
  default     = ""
}

