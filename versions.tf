provider "aws" {
  alias = "log_account"
}

provider "aws" {
  alias = "prod_account"
}

terraform {
  required_version = ">= 0.13.0"

  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = ">= 2.0.0"
      configuration_aliases = [aws.log_account, aws.prod_account]
    }
  }
}
